# Use the official MongoDB image as the base image
FROM python:3.10

# Install required packages (MongoDB dependencies)
RUN apt-get update

RUN apt-get install -y vim sudo npm python3-pip
# Create a directory for MongoDB data storage
RUN mkdir -p /data/db

# Expose the default MongoDB port
EXPOSE 27017

# Create a symbolic link to make python3 as default python interpreter
RUN ln -s /usr/bin/python3.9 /usr/bin/python

# FROM python:3.9.6

# RUN useradd user
# RUN mkdir /home/user

# # Define your working directory
WORKDIR /var/task

RUN chmod +w /etc/sudoers
RUN echo 'user ALL=(ALL) NOPASSWD:ALL' | tee -a /etc/sudoers
RUN chmod -w /etc/sudoers

COPY ./requirements.txt ./
RUN pip install --upgrade pip
RUN pip install -r ./requirements.txt



