#!/usr/bin/env bash

image=$1

if [ "$image" == "" ]
then
    image=python:3.10
fi

# 포트 바인딩
docker run -p 8080:5000  -v $(pwd)/task:/var/task --rm -it $image /bin/bash
