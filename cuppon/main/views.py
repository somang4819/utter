from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
    return render(request,'main/index.html')

def main(request):
    message = request.GET.get('abc')
    print(message)

    return HttpResponse("안녕?")