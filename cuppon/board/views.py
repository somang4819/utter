from django.shortcuts import render

# Create your views here.
from .models import Blog
from .serializer import BlogSerializer
from rest_framework import viewsets

from collections import Counter

# Blog의 목록, detail 보여주기, 수정하기, 삭제하기 모두 가능
class BlogViewSet(viewsets.ModelViewSet):
    queryset = Blog.objects.all()
    serializer_class = BlogSerializer
   
   	# # serializer.save() 재정의
    def perform_create(self, serializer):
        print("perform_create")
        #  * 연관게시글은 게시글의 내용을 단어별로 공백기준으로  나눠서 각 단어가 다른 게시글에서 얼마나 많이 나타나는지를 기준으로 합니다.
        print(self.request.data)

        words =self.request.data['body'].split()
        words_count = Counter(words)
        #most_common_words = words_count.most_common()

        # 빈도수가 2번 이상 나타난 요소만 가져오기
        most_common_words = [(word, count) for word, count in words_count.items() if count >= 2]

        print(most_common_words) 
        #self.request.data['keywords']=most_common_words[:10]
        #self.request.data['keywords']="test keywords"
        instance = serializer.save(keywords=most_common_words[:10])

        # related_post= None
        for word in most_common_words[:10]:
            print(word)
            related_post = Blog.objects.filter(keywords__contains=word).exclude(title=self.request.data['title']).first()
            if related_post:
                        instance.related_post.add(related_post)

        # # res = instance.keywords.set(most_common_words[:10])
        # print(res)



        #  * 연관게시글이 되는 기준은 전체 게시글에 존재하는 단어를 기준으로 40% 이하 빈도 단어가 두개이상 동시에 나타나는 것입니다.

        #  * 게시글이 생성되면 연관게시글을 찾아서 연결합니다.
        
        #  * 단, 문장에 자주쓰이는 단어를 배제하기 위해서 전체게시글 중에 60%이상에서 발견되는 단어는 연관게시글을 파악할때 사용하지 않습니다.
        #  * 그리고, 게시글에 40% 이하 빈도로 나타나는 단어는 해당게시글에서 자주 나타날수록 더 연관이 있는 것으로 계산합니다.
        #  * 마지막으로 연관게시글에서 위에서 계산한 40% 이하 빈도로 나타나는 단어중 더 빈번하게 나타날수록 연관이 더 있는것으로 파악하고, 연관도가 높은 순서대로 연관게시글을 보여줍니다.
